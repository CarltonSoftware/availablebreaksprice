# Available Breaks Price

This is a utility module which can be used to calculate available breaks prices from the tabs2 api.

To install:

```npm install https://bitbucket.org/CarltonSoftware/availablebreaksprice```

To test:

```npm test```
const assert = require('chai').assert;
const prices = require('./prices.json');
const GetAvailableBreaksPrice = require('../').default;
const Utils = require('../').Utils;
const priceTests = [
  {
    date: '2021-03-07',
    days: 7,
    price: 371
  },
  {
    date: '2021-03-08',
    days: 7,
    price: 371
  },
  {
    date: '2021-05-15',
    days: 7,
    price: 460
  },
  {
    date: '2021-05-12',
    days: 7,
    price: undefined
  },
  {
    date: '2021-05-15',
    days: 14,
    price: 952
  },
  {
    date: '2021-04-17',
    days: 3,
    price: 360
  },
  {
    date: '2021-03-06',
    days: 18,
    price: 953
  }
];

describe('Test Available Breaks Pricing', function() {
  it('should be a function', function() {
    assert.equal(true, typeof GetAvailableBreaksPrice === 'function');
  });

  priceTests.forEach(function(test) {
    it('should return a correct price for a ' + test.days + ' day period (' + test.price + ' on ' + Utils._formatDate(test.date, 'PPP') + ')', function() {
      assert.equal(test.price, GetAvailableBreaksPrice(prices, test.date, test.days));
    });
  });
});

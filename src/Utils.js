var isSameDay = require('date-fns').isSameDay;
var parse = require('date-fns').parse;
var format = require('date-fns').format;

module.exports._parseDate = function(dateString) {
  if (typeof dateString === 'string') {
    return parse(dateString, 'yyyy-LL-dd', new Date());
  }

  if (dateString instanceof Date) {
    return dateString;
  }
};

module.exports._isSameDay = function(date, dateString) {
  return isSameDay(date, module.exports._parseDate(dateString));
};

module.exports._formatPeriod = function(date) {
  return format(date, 'yyyyLL');
};

module.exports._formatDate = function(date, dateFormat) {
  return format(module.exports._parseDate(date), dateFormat || 'yyyy-LL-dd');
};
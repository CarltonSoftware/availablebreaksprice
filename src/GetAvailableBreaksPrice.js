var parse = require('date-fns').parse;
var addDays = require('date-fns').addDays;
var Utils = require('./Utils');

function GetAvailableBreaksPrice(breakprices, fromDate, days) {
  fromDate = Utils._parseDate(fromDate);
  days = days || 7;

  var prices = breakprices.filter(function(p) {
    return Utils._isSameDay(fromDate, p.date);
  });

  if (prices.length > 0) {
    let price = prices.filter(
      function(p) {
        return p.days === days;
      }
    );

    if (days <= 7) {
      if (price.length >= 1) {
        return price.pop().price;
      }
    }

    if (days > 7) {
      let getPrice = function(prices, availablebreakprices, fromDate, days) {
        days = days || 7;
        let price = availablebreakprices.filter(
          function(p) {
            return p.days === days && Utils._isSameDay(fromDate, p.date);
          }
        );

        if (price.length === 1) {
          prices.push(price.shift().price);
        } else {
          prices.push(-1);
        }
      };

      var _prices = [];
      let add = days % 7;
      let weeks = (days - add) / 7;

      if (days < 14) {
        add = days;
      } else if (add > 0) {
        add = add + 7;
      }

      let to = addDays(fromDate, 7);
      for (let i = 0; i < weeks; i++) {
        to = addDays(fromDate, i * 7);
        getPrice(_prices, breakprices, to, 7);
      }

      if (to && add > 0) {
        getPrice(_prices, breakprices, to, add);
      }

      if (_prices.indexOf(-1) < 0) {
        let total = 0;
        _prices.forEach(function(p) {
          total += p;
        });

        return total;
      } else {
        return 0;
      }
    }

    return 0;
  }
};

module.exports = GetAvailableBreaksPrice;